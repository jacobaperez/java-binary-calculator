package project;

import java.util.*;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * Helper functions to help data easier to work with.
 */
public class Helpers {
	String OPERANDS = "*/+-";

	public ArrayList<String> condenseEntries(String[] array) {
		ArrayList<String> newArray = new ArrayList<String>();
		String value = "";
		for (int i = 0; i < array.length; i++) {
			if (OPERANDS.contains(array[i])) {
				newArray.add(value);
				value = "";
				newArray.add(array[i]);
			} else {
				value += array[i];
			}
		}
		if (value != "") {
			newArray.add(value);
		}
		return newArray;
	}

	public ArrayList<String> convertBaseArray(ArrayList<String> arr, String mode) {
		ArrayList<String> entries = new ArrayList<String>();

		if (mode.equalsIgnoreCase("HEX")) {
			for (int i = 0; i < arr.size(); i++) {
				String value = arr.get(i);
				if (OPERANDS.contains(value)) {
					entries.add(value);
				} else {
					int decimal = Integer.parseInt(value, 16);
					entries.add(Integer.toString(decimal));
				}
			}
			return entries;

		} else if (mode.equalsIgnoreCase("DEC")) {
			for (int i = 0; i < arr.size(); i++) {
				String value = arr.get(i);
				if (OPERANDS.contains(value)) {
					entries.add(value);
				} else {
					int decimal = Integer.parseInt(value, 10);
					entries.add(Integer.toString(decimal));
				}
			}
			return entries;

		} else if (mode.equalsIgnoreCase("OCT")) {
			for (int i = 0; i < arr.size(); i++) {
				String value = arr.get(i);
				if (OPERANDS.contains(value)) {
					entries.add(value);
				} else {
					int decimal = Integer.parseInt(value, 8);
					entries.add(Integer.toString(decimal));
				}
			}
			return entries;

		} else {
			for (int i = 0; i < arr.size(); i++) {
				String value = arr.get(i);
				if (OPERANDS.contains(value)) {
					entries.add(value);
				} else {
					int decimal = Integer.parseInt(value, 2);
					entries.add(Integer.toString(decimal));
				}
			}
			return entries;
		}

	}

	public boolean checkForDivideByZero(ArrayList<String> arr) {
		for (int i = 2; i < arr.size(); i+=2) {
			int value = Integer.parseInt(arr.get(i));
			if (value == 0 && arr.get(i - 1).equalsIgnoreCase("/")) {
				return true;
			}
		}
		return false;
	}

	public String solveExpression(ArrayList<String> entries, String mode) {
		String exp = "";
		for (int i = 0; i < entries.size(); i++) {
			exp += entries.get(i);
		}

		ScriptEngineManager mgr = new ScriptEngineManager();
		ScriptEngine engine = mgr.getEngineByName("JavaScript");
		try {
			if (engine.eval(exp) instanceof Double) {
				Double v = new Double((double) engine.eval(exp));
				int value = v.intValue();
				String result;
				if (mode.equalsIgnoreCase("HEX")) {
					result = Integer.toString(value, 16);
				} else if (mode.equalsIgnoreCase("DEC")) {
					result = Integer.toString(value, 10);
				} else if (mode.equalsIgnoreCase("OCT")) {
					result = Integer.toString(value, 8);
				} else {
					result = Integer.toString(value, 2);
				}
				return result;
			} else {
				if (mode.equalsIgnoreCase("HEX")) {
					return Integer.toString((int) engine.eval(exp), 16);
				} else if (mode.equalsIgnoreCase("DEC")) {
					return Integer.toString((int) engine.eval(exp), 10);
				} else if (mode.equalsIgnoreCase("OCT")) {
					return Integer.toString((int) engine.eval(exp), 8);
				} else {
					return Integer.toString((int) engine.eval(exp), 2);
				}
			}
		} catch (ScriptException e) {
			e.printStackTrace();
			return "Error";
		}
	}

}
