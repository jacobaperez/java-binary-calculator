package project;

import javax.swing.*;

/**
 * 
 * @author Jake Perez
 * Small abstraction to create multiple disabled buttons easily.
 */
public class DisabledButton {
	
	public JButton Button (String value) {
		JButton b = new JButton(value);
		b.setEnabled(false);
		return b;
	}
}
