package project;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class JavaCalculator extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String OPERANDS = "*/+-";

	private JButton btnNumber0 = new JButton("0");
	private JButton btnNumber1 = new JButton("1");
	private JButton btnNumber2 = new JButton("2");
	private JButton btnNumber3 = new JButton("3");
	private JButton btnNumber4 = new JButton("4");
	private JButton btnNumber5 = new JButton("5");
	private JButton btnNumber6 = new JButton("6");
	private JButton btnNumber7 = new JButton("7");
	private JButton btnNumber8 = new JButton("8");
	private JButton btnNumber9 = new JButton("9");
	private JButton btnAdd = new JButton("+");
	private JButton btnSubtract = new JButton("-");
	private JButton btnMultiply = new JButton("×");
	private JButton btnDivide = new JButton("÷");
	private JButton btnClear = new JButton("CE");
	private JButton btnDelete = new JButton("Del");
	private JButton btnEqual = new JButton("=");
	private JButton btnHex = new JButton("HEX");
	private JButton btnDec = new JButton("DEC");
	private JButton btnOct = new JButton("OCT");
	private JButton btnBin = new JButton("BIN");

	private JButton btnA = new JButton("A");
	private JButton btnB = new JButton("B");
	private JButton btnC = new JButton("C");
	private JButton btnD = new JButton("D");
	private JButton btnE = new JButton("E");
	private JButton btnF = new JButton("F");

	private String mode = "HEX";
	ArrayList<String> entries = new ArrayList<String>();
	private JTextField textFieldResult;
	String display = "";

	public JavaCalculator() {
		JPanel p3 = new JPanel();
		p3.setLayout(new GridLayout(4, 1));
		p3.add(btnHex);
		p3.add(btnDec);
		p3.add(btnOct);
		p3.add(btnBin);

		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(6, 6));

		p1.add(new DisabledButton().Button("Lsh"));
		p1.add(new DisabledButton().Button("Rsh"));
		p1.add(new DisabledButton().Button("Or"));
		p1.add(new DisabledButton().Button("Xor"));
		p1.add(new DisabledButton().Button("Not"));
		p1.add(new DisabledButton().Button("And"));

		p1.add(new DisabledButton().Button("\u2191"));
		p1.add(new DisabledButton().Button("Mod"));
		p1.add(btnClear);
		p1.add(new DisabledButton().Button("C"));
		p1.add(btnDelete);
		p1.add(btnDivide);

		p1.add(btnA);
		p1.add(btnB);
		p1.add(btnNumber7);
		p1.add(btnNumber8);
		p1.add(btnNumber9);
		p1.add(btnMultiply);

		p1.add(btnC);
		p1.add(btnD);
		p1.add(btnNumber4);
		p1.add(btnNumber5);
		p1.add(btnNumber6);
		p1.add(btnSubtract);

		p1.add(btnE);
		p1.add(btnF);
		p1.add(btnNumber1);
		p1.add(btnNumber2);
		p1.add(btnNumber3);
		p1.add(btnAdd);

		p1.add(new DisabledButton().Button("("));
		p1.add(new DisabledButton().Button(")"));
		p1.add(new DisabledButton().Button("±"));
		p1.add(btnNumber0);
		p1.add(new DisabledButton().Button("."));
		p1.add(btnEqual);

		JPanel p2 = new JPanel();
		p2.setLayout(new FlowLayout());
		p2.add(textFieldResult = new JTextField(30));

		JPanel fullPanel = new JPanel();
		fullPanel.setLayout(new BorderLayout());
		fullPanel.add(p2, BorderLayout.NORTH);
		fullPanel.add(p3, BorderLayout.WEST);
		fullPanel.add(p1, BorderLayout.CENTER);

		add(fullPanel);

		btnNumber1.addActionListener(new ListenForOne());
		btnNumber0.addActionListener(new ListenForZero());
		btnNumber2.addActionListener(new ListenForTwo());
		btnNumber3.addActionListener(new ListenForThree());
		btnNumber4.addActionListener(new ListenForFour());
		btnNumber5.addActionListener(new ListenForFive());
		btnNumber6.addActionListener(new ListenForSix());
		btnNumber7.addActionListener(new ListenForSeven());
		btnNumber8.addActionListener(new ListenForEight());
		btnNumber9.addActionListener(new ListenForNine());
		btnA.addActionListener(new ListenForA());
		btnB.addActionListener(new ListenForB());
		btnC.addActionListener(new ListenForC());
		btnD.addActionListener(new ListenForD());
		btnE.addActionListener(new ListenForE());
		btnF.addActionListener(new ListenForF());
		btnClear.addActionListener(new ListenForClear());
		btnAdd.addActionListener(new ListenForAdd());
		btnDelete.addActionListener(new ListenForDelete());
		btnMultiply.addActionListener(new ListenForMultiply());
		btnDivide.addActionListener(new ListenForDivide());
		btnSubtract.addActionListener(new ListenForSubtract());
		btnEqual.addActionListener(new ListenForSolve());

		btnHex.addActionListener(new ListenForHex());
		btnDec.addActionListener(new ListenForDec());
		btnOct.addActionListener(new ListenForOct());
		btnBin.addActionListener(new ListenForBin());

		textFieldResult.addActionListener(new ListenForEnter());

	}

	public static void main(String[] args) {
		JavaCalculator calc = new JavaCalculator();
		calc.pack();
		calc.setLocationRelativeTo(null);
		calc.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		calc.setVisible(true);
	}

	class ListenForA implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			display = textFieldResult.getText();
			if (display.equals("Error")) {
				textFieldResult.setText("A");
			} else {
				textFieldResult.setText(display + "A");
			}
		}
	}
	
	class ListenForB implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			display = textFieldResult.getText();
			if (display.equals("Error")) {
				textFieldResult.setText("B");
			} else {
				textFieldResult.setText(display + "B");
			}
		}
	}
	
	class ListenForC implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			display = textFieldResult.getText();
			if (display.equals("Error")) {
				textFieldResult.setText("C");
			} else {
				textFieldResult.setText(display + "C");
			}
		}
	}
		
	class ListenForD implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			display = textFieldResult.getText();
			if (display.equals("Error")) {
				textFieldResult.setText("D");
			} else {
				textFieldResult.setText(display + "D");
			}
		}
	}
	
	class ListenForE implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			display = textFieldResult.getText();
			if (display.equals("Error")) {
				textFieldResult.setText("E");
			} else {
				textFieldResult.setText(display + "E");
			}
		}
	}
	
	class ListenForF implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			display = textFieldResult.getText();
			if (display.equals("Error")) {
				textFieldResult.setText("F");
			} else {
				textFieldResult.setText(display + "F");
			}
		}
	}
	
	class ListenForOct implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			entries.clear();
			btnA.setEnabled(false);
			btnB.setEnabled(false);
			btnC.setEnabled(false);
			btnD.setEnabled(false);
			btnE.setEnabled(false);
			btnF.setEnabled(false);
			btnNumber2.setEnabled(true);
			btnNumber3.setEnabled(true);
			btnNumber4.setEnabled(true);
			btnNumber5.setEnabled(true);
			btnNumber6.setEnabled(true);
			btnNumber7.setEnabled(true);
			btnNumber8.setEnabled(false);
			btnNumber9.setEnabled(false);

			mode = "OCT";
			textFieldResult.setText("");
		}
	}

	class ListenForDec implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			entries.clear();
			btnA.setEnabled(false);
			btnB.setEnabled(false);
			btnC.setEnabled(false);
			btnD.setEnabled(false);
			btnE.setEnabled(false);
			btnF.setEnabled(false);
			btnNumber2.setEnabled(true);
			btnNumber3.setEnabled(true);
			btnNumber4.setEnabled(true);
			btnNumber5.setEnabled(true);
			btnNumber6.setEnabled(true);
			btnNumber7.setEnabled(true);
			btnNumber8.setEnabled(true);
			btnNumber9.setEnabled(true);

			mode = "DEC";
			textFieldResult.setText("");
		}
	}

	class ListenForHex implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			entries.clear();
			btnA.setEnabled(true);
			btnB.setEnabled(true);
			btnC.setEnabled(true);
			btnD.setEnabled(true);
			btnE.setEnabled(true);
			btnF.setEnabled(true);
			btnNumber2.setEnabled(true);
			btnNumber3.setEnabled(true);
			btnNumber4.setEnabled(true);
			btnNumber5.setEnabled(true);
			btnNumber6.setEnabled(true);
			btnNumber7.setEnabled(true);
			btnNumber8.setEnabled(true);
			btnNumber9.setEnabled(true);

			mode = "HEX";
			textFieldResult.setText("");
		}
	}

	class ListenForBin implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			entries.clear();
			btnA.setEnabled(false);
			btnB.setEnabled(false);
			btnC.setEnabled(false);
			btnD.setEnabled(false);
			btnE.setEnabled(false);
			btnF.setEnabled(false);
			btnNumber2.setEnabled(false);
			btnNumber3.setEnabled(false);
			btnNumber4.setEnabled(false);
			btnNumber5.setEnabled(false);
			btnNumber6.setEnabled(false);
			btnNumber7.setEnabled(false);
			btnNumber8.setEnabled(false);
			btnNumber9.setEnabled(false);

			mode = "BIN";
			textFieldResult.setText("");
		}
	}

	class ListenForEnter implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			display = textFieldResult.getText();
			if (display.length() > 0 && !display.equals("Error")) {
				String lastValue = display.substring(display.length() - 1, display.length());
				if (!OPERANDS.contains(lastValue)) {
					boolean divideByZero;
					entries = new Helpers().convertBaseArray(new Helpers().condenseEntries(display.split("")), mode);
					divideByZero = new Helpers().checkForDivideByZero(entries);
					if (divideByZero) {
						entries.clear();
						textFieldResult.setText("Error");
					} else {
						String solution = new Helpers().solveExpression(entries, mode);
						entries.clear();
						textFieldResult.setText(solution);						
					}
				} else {
					entries.clear();
					textFieldResult.setText("Error");
				}
			}
		}
	}

	class ListenForOne implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			display = textFieldResult.getText();
			if (display.equals("Error")) {
				textFieldResult.setText("1");
			} else {
				textFieldResult.setText(display + "1");
			}
		}
	}

	class ListenForZero implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			display = textFieldResult.getText();
			if (display.equals("Error")) {
				textFieldResult.setText("0");
			} else {
				textFieldResult.setText(display + "0");
			}
		}
	}

	class ListenForTwo implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			display = textFieldResult.getText();
			if (display.equals("Error")) {
				textFieldResult.setText("2");
			} else {
				textFieldResult.setText(display + "2");
			}
		}
	}

	class ListenForThree implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			display = textFieldResult.getText();
			if (display.equals("Error")) {
				textFieldResult.setText("3");
			} else {
				textFieldResult.setText(display + "3");
			}
		}
	}

	class ListenForFour implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			display = textFieldResult.getText();
			if (display.equals("Error")) {
				textFieldResult.setText("4");
			} else {
				textFieldResult.setText(display + "4");
			}
		}
	}

	class ListenForFive implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			display = textFieldResult.getText();
			if (display.equals("Error")) {
				textFieldResult.setText("5");
			} else {
				textFieldResult.setText(display + "5");
			}
		}
	}

	class ListenForSix implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			display = textFieldResult.getText();
			if (display.equals("Error")) {
				textFieldResult.setText("6");
			} else {
				textFieldResult.setText(display + "6");
			}
		}
	}

	class ListenForSeven implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			display = textFieldResult.getText();
			if (display.equals("Error")) {
				textFieldResult.setText("7");
			} else {
				textFieldResult.setText(display + "7");
			}
		}
	}

	class ListenForEight implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			display = textFieldResult.getText();
			if (display.equals("Error")) {
				textFieldResult.setText("8");
			} else {
				textFieldResult.setText(display + "8");
			}
		}
	}
	
	class ListenForNine implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			display = textFieldResult.getText();
			if (display.equals("Error")) {
				textFieldResult.setText("9");
			} else {
				textFieldResult.setText(display + "9");
			}
		}
	}

	class ListenForClear implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			textFieldResult.setText("");
		}
	}

	class ListenForAdd implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			display = textFieldResult.getText();
			if (display.length() > 0) {
				String lastValue = display.substring(display.length() - 1, display.length());
				if (!OPERANDS.contains(lastValue) && !display.equals("Error")) {
					textFieldResult.setText(display + "+");
				}
			}
		}
	}

	class ListenForDelete implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			display = textFieldResult.getText();
			if (display.equals("Error")) {
				textFieldResult.setText("");
			} else if (display.length() > 0) {
				display = display.substring(0, display.length() - 1);
				textFieldResult.setText(display);
			}
		}
	}

	class ListenForSubtract implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			display = textFieldResult.getText();
			if (display.length() > 0 && !display.equals("Error")) {
				String lastValue = display.substring(display.length() - 1, display.length());
				if (!OPERANDS.contains(lastValue)) {
					textFieldResult.setText(display + "-");
				}
			}
		}
	}

	class ListenForMultiply implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			display = textFieldResult.getText();
			if (display.length() > 0 && !display.equals("Error")) {
				String lastValue = display.substring(display.length() - 1, display.length());
				if (!OPERANDS.contains(lastValue)) {
					textFieldResult.setText(display + "*");
				}
			}
		}
	}

	class ListenForDivide implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			display = textFieldResult.getText();
			if (display.length() > 0 && !display.equals("Error")) {
				String lastValue = display.substring(display.length() - 1, display.length());
				if (!OPERANDS.contains(lastValue)) {
					textFieldResult.setText(display + "/");
				}
			}
		}
	}

	class ListenForSolve implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			display = textFieldResult.getText();
			if (display.length() > 0 && !display.equals("Error")) {
				String lastValue = display.substring(display.length() - 1, display.length());
				if (!OPERANDS.contains(lastValue)) {
					boolean divideByZero;
					entries = new Helpers().convertBaseArray(new Helpers().condenseEntries(display.split("")), mode);
					divideByZero = new Helpers().checkForDivideByZero(entries);
					if (divideByZero) {
						entries.clear();
						textFieldResult.setText("Error");
					} else {
						String solution = new Helpers().solveExpression(entries, mode);
						entries.clear();
						textFieldResult.setText(solution);						
					}
				} else {
					entries.clear();
					textFieldResult.setText("Error");
				}
			}
		}
	}

}